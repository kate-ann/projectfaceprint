/* ==========================================================================

    Project: Website name
    Last updated: August 2017

   ========================================================================== */

(function($) {

	'use strict';
	
	
	var Main = {
	
	  /**
	   * Init function
	   */
	  init: function() {
	    Main.initMenu();
	    
	  },
	
	  /*
	   * Init menu
	   */
	  initMenu: function(){
	  	
	  	$('.menu-item-has-children').append('<div class="c-nav__open-menu-btn js-nav__open-menu-btn"></div>');
			
			$('.sub-menu, .js-nav__menu').velocity('slideUp', { duration: 0 });
			
			$('.js-nav__btn').click(function(){
				var isOpen = $('.js-nav__menu').is(':visible'),direction = isOpen ? 'slideUp' : 'slideDown';
				$('.js-nav__menu').velocity(direction, { duration: 300, easing: 'ease' });
			});
		
			$('.js-nav__open-menu-btn').click(function(){
				var submenu = $(this).parent().find('.sub-menu');
				
				if($(submenu).hasClass('toggled')){
					$(submenu).velocity('slideUp', { duration: 300, easing: 'ease' });
					$(submenu).removeClass('toggled');
				
				} else {
					$(submenu).velocity('slideDown', { duration: 300, easing: 'ease' });
					$(submenu).addClass('toggled');
				}
				
				$(this).toggleClass('close');
			});		  	
	  }	  
	};
	
	document.addEventListener('DOMContentLoaded', function() {
		Main.init();
	});
})( jQuery );