<?php
/**
* Template Name: Landing
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_landing_logo' ) ||
					 get_field( 'x5_landing_intro_heading' ) ||
					 get_field( 'x5_landing_intro_desc' ) ||
					 get_field( 'x5_landing_intro_form_is' ) ||
					 get_field( 'x5_landing_intro_bg_m' ) ||
					 get_field( 'x5_landing_intro_bg_t' ) ||
					 get_field( 'x5_landing_intro_bg_d' ) ): ?>

	<section class="c-intro">

		<div class="container px-lg-0">

			<div class="row mx-lg-0 justify-content-center justify-content-lg-start">

				<div class="col-lg-6 px-lg-0">

					<?php if ( get_field( 'x5_landing_logo' ) ): ?>
						<h1 class="c-logo"><a href="<?php echo esc_url( home_url() ); ?>" rel="index" title="<?php esc_html_e( 'Go to homepage', 'x5' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
					<?php endif; ?>

					<?php if ( get_field( 'x5_landing_intro_heading' ) ||
										 get_field( 'x5_landing_intro_desc' ) ): ?>

						<div class="info">

							<?php if ( get_field( 'x5_landing_intro_heading' ) ): ?>
								<h2 class="heading"><?php echo esc_html( get_field( 'x5_landing_intro_heading' ) ); ?></h2 >
							<?php endif; ?>

							<?php if ( get_field( 'x5_landing_intro_desc' ) ): ?>
								<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_landing_intro_desc' ) ) ); ?></div>
							<?php endif; ?>

						</div>
						<!-- info -->

					<?php endif; ?>

					<?php if ( get_field( 'x5_landing_intro_form_is' ) &&
										 get_field( 'x5_landing_intro_form_code' ) ): ?>
						<?php echo get_field( 'x5_landing_intro_form_code' ); ?>
					<?php endif; ?>

				</div>
				<!-- col -->
			</div>
			<!-- row -->

		</div>
		<!-- container -->

	</section>
	<!-- c-intro -->

<?php endif; ?>


<?php if ( have_rows( 'x5_landing_about_rows' ) ): ?>

	<section class="c-about">

		<div class="container px-lg-0">

			<div class="row mx-lg-0 justify-content-center justify-content-lg-start columns">
				<?php while( have_rows( 'x5_landing_about_rows' ) ) : the_row(); ?>

					<?php if ( get_sub_field( 'x5_landing_about_rows_icon' ) ||
										 get_sub_field( 'x5_landing_about_rows_desc' ) ): ?>

						<div class="col-lg-6">

							<?php if ( get_sub_field( 'x5_landing_about_rows_desc' ) ): ?>
								<?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_landing_about_rows_desc' ) ) ); ?>
							<?php endif; ?>

						</div>
						<!-- col -->

					<?php endif; ?>

				<?php endwhile; ?>

			</div>
			<!-- row -->

		</div>
		<!-- container -->

	</section>
	<!-- c-about -->

<?php endif; ?>


<?php get_footer();
