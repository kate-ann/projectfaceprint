<?php

// Landing page
if ( is_page_template( 'page-landing.php' ) ):

	// Header logo
	if ( get_field( 'x5_landing_logo' ) &&
		 	 get_field( 'x5_landing_logow' ) &&
		 	 get_field( 'x5_landing_logoh' ) ):
		// SVG image
		$x5_landing_logo = get_field( 'x5_landing_logo' ); ?>

		.page-landing .c-intro .c-logo {
			width: <?php echo wp_filter_nohtml_kses( get_field( 'x5_landing_logow' ) ); ?>;
			height: <?php echo wp_filter_nohtml_kses( get_field( 'x5_landing_logoh' ) ); ?>;
		  background: transparent url("<?php echo esc_url( $x5_landing_logo['url'] ); ?>") 0 0 no-repeat;
		  background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_landing_logow' ) ); ?> auto;
		}

		<?php
	elseif ( get_field( 'x5_landing_logo' ) ):
		// PNG/Jpeg image
		$x5_landing_logo = get_field( 'x5_landing_logo' ); ?>

		.page-landing .c-intro .c-logo {
			width: <?php echo wp_filter_nohtml_kses( $x5_landing_logo['width'] ); ?>px;
			height: <?php echo wp_filter_nohtml_kses( $x5_landing_logo['height'] ); ?>px;
		  background: transparent url("<?php echo esc_url( $x5_landing_logo['url'] ); ?>") 0 0 no-repeat;
		}

		<?php

	endif;

	// Intro section
	if ( get_field( 'x5_landing_intro_bg_m' ) &&
			 get_field( 'x5_landing_intro_bg_t' ) &&
			 get_field( 'x5_landing_intro_bg_d' ) ):
		$x5_landing_intro_bg_m = get_field( 'x5_landing_intro_bg_m' );
		$x5_landing_intro_bg_t = get_field( 'x5_landing_intro_bg_t' );
		$x5_landing_intro_bg_d = get_field( 'x5_landing_intro_bg_d' );
		?>
		.page-landing .c-intro {
		  background: #2e3192 url('<?php echo esc_url( $x5_landing_intro_bg_m['url'] ); ?>') 0 100% repeat-x;
		}
		@media screen and (min-width: 768px) {
		  .page-landing .c-intro {
		    background: #2e3192 url('<?php echo esc_url( $x5_landing_intro_bg_t['url'] ); ?>') 0 100% repeat-x;
		  }
		}
		@media screen and (min-width: 992px) {
		  .page-landing .c-intro {
		    background: #2e3192 url('<?php echo esc_url( $x5_landing_intro_bg_d['url'] ); ?>') 100% center no-repeat;
		    background-size: contain;
		  }
		}
		<?php
	endif;
	// end of Intro section


	// About section
	if ( have_rows( 'x5_landing_about_rows' ) ):
		$x5_landing_about_rows_count = 1;

		while( have_rows( 'x5_landing_about_rows' ) ) : the_row();

			if ( get_sub_field( 'x5_landing_about_rows_icon' ) ):
				$x5_landing_about_rows_icon = get_sub_field( 'x5_landing_about_rows_icon' );
				?>
				.page-landing .c-about [class^="col-"]:nth-child(<?php echo esc_html( $x5_landing_about_rows_count ); ?>) {
				  background: transparent url('<?php echo esc_url( $x5_landing_about_rows_icon['url'] ); ?>') 15px 0 no-repeat;
				}
				<?php
			endif;

			if ( get_sub_field( 'x5_landing_about_rows_iconw_m' ) ):
				$x5_landing_about_rows_iconw_m = get_sub_field( 'x5_landing_about_rows_iconw_m' );
				?>
				.page-landing .c-about [class^="col-"]:nth-child(<?php echo esc_html( $x5_landing_about_rows_count ); ?>) {
				  background-size: <?php echo esc_html( $x5_landing_about_rows_iconw_m ); ?> auto;
				}
				<?php
			endif;

			if ( get_sub_field( 'x5_landing_about_rows_iconw_t' ) ):
				$x5_landing_about_rows_iconw_t = get_sub_field( 'x5_landing_about_rows_iconw_t' );
				?>
				@media screen and (min-width: 768px) {
					.page-landing .c-about [class^="col-"]:nth-child(<?php echo esc_html( $x5_landing_about_rows_count ); ?>) {
					  background-size: <?php echo esc_html( $x5_landing_about_rows_iconw_t ); ?> auto;
					}
				}
				<?php
			endif;

			if ( get_sub_field( 'x5_landing_about_rows_iconw_d' ) ):
				$x5_landing_about_rows_iconw_d = get_sub_field( 'x5_landing_about_rows_iconw_d' );
				?>
				@media screen and (min-width: 992px) {
					.page-landing .c-about [class^="col-"]:nth-child(<?php echo esc_html( $x5_landing_about_rows_count ); ?>) {
					  background-size: <?php echo esc_html( $x5_landing_about_rows_iconw_d ); ?> auto;
					}
				}
				<?php
			endif;

			$x5_landing_about_rows_count++;
		endwhile;

	endif;

endif;
// end of Landing page