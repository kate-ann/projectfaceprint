<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package x5
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">
	<?php

		if ( have_comments() ) : ?>
			<h2 class="comments-title">
				<?php
					printf( // WPCS: XSS OK.
						esc_html( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'x5' ) ),
						number_format_i18n( get_comments_number() ),
						'<span>' . get_the_title() . '</span>'
					);
				?>
			</h2>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
				<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'x5' ); ?></h2>
				<div class="nav-links">

					<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'x5' ) ); ?></div>
					<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'x5' ) ); ?></div>

				</div><!-- .nav-links -->
			</nav><!-- #comment-nav-above -->
			<?php endif; // Check for comment navigation. ?>

			<ol class="comment-list">
				<?php
					wp_list_comments( array(
						'style'			=> 'ol',
						'short_ping' => true,
					) );
				?>
			</ol><!-- .comment-list -->

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
				<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'x5' ); ?></h2>
				<div class="nav-links">

					<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'x5' ) ); ?></div>
					<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'x5' ) ); ?></div>

				</div><!-- .nav-links -->
			</nav><!-- #comment-nav-below -->
			<?php
			endif; // Check for comment navigation.

		endif; // Check for have_comments().


		// If comments are closed and there are comments, let's leave a little note, shall we?
		if (
			! comments_open() &&
			get_comments_number() &&
			post_type_supports( get_post_type(), 'comments' ) ) : ?>

			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'x5' ); ?></p>
		<?php
		endif;

		$comments_args = array(
			'fields' => array(
				'author' => '
					<div class="form-group name">
						<label for="author" class="u-hidden-visually">Name</label>
						<input id="author" class="form-control name-txt" type="text" name="author" value="" placeholder="Name" />
					</div>',
				'email' => '
					<div class="form-group email">
						<label for="email" class="u-hidden-visually">Email</label>
						<input id="email" class="form-control email-txt" type="email" name="email" value="" placeholder="Email" />
					</div>',
				'url' => '
					<div class="form-group website">
						<label for="url" class="u-hidden-visually">WEBSITE</label>
						<input id="url" class="form-control url-txt" type="url" name="url" value="" placeholder="Website" />
					</div>'
			),

		      'label_submit' => __( 'POST COMMENT', 'x5' ),
		      'title_reply' => __( 'LEAVE A REPLY', 'x5' ),
		      'comment_field' => '
		        <div class="form-group comment">
		        	<label for="comment" class="u-hidden-visually">Comment</label>
		        	<textarea id="comment" class="form-control message-txt" cols="40" rows="8" placeholder="Comment" name="comment" ></textarea>
		        </div>',
		      'comment_notes_before' => '<p class="comments-notes">Your email address will not be published.</p>',
		);

		comment_form( $comments_args );

	?>
</div>
<!-- / comments -->
