<?php

/*
 * X5: Page
 *
 * @package WordPress
 * @subpackage X5
 */
get_header();
the_post();
?>

<div class="c-content">
	<div class="o-container">
		<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
	</div>
</div>

<?php get_footer();
