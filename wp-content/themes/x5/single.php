<?php
/**
 * X5: Post
 *
 * @package WordPress
 * @subpackage X5
 */
get_header();
?>

	<?php	while ( have_posts() ) : the_post(); ?>

		<article class="c-post content">

			<div class="o-container">

		    <section class="entry-content">

	      	<?php the_content(); ?>

	      	<?php echo esc_html( 'Written by ' ); echo get_the_author(); ?>

		    </section>
		    <!-- / entry-content -->

			</div>
			<!-- o-container -->

	  </article>
		<!-- / c-post -->

		<?php // If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		?>

	<?php endwhile; ?>

<?php get_footer();

