<?php
/**
 * X5: Footer
 *
 * Remember to always include the wp_footer() call before the </body> tag
 *
 * @package WordPress
 * @subpackage X5
 */
?>

<?php if ( have_rows( 'x5_footer_social_btns', 'option' ) ||
					 get_field( 'x5_footer_copyright', 'option' ) ): ?>

	<footer class="c-footer">

	  <div class="container">

	  	<div class="row justify-content-center">
	  		<div class="col-12">

	  			<?php get_template_part( 'partials/social', 'buttons' ); ?>

					<?php if ( get_field( 'x5_footer_copyright', 'option' ) ): ?>
						<p class="copyright"><?php echo esc_html( get_field( 'x5_footer_copyright', 'option' ) ); ?></p>
					<?php endif; ?>

	  		</div>
	  		<!-- col -->
	  	</div>
	  	<!-- row -->

	  </div>
	  <!-- container -->

	</footer>
	<!-- c-footer -->

<?php endif; ?>


<?php
	// do not remove
	wp_footer();
?>

</body>
</html>
